<?php

use App\Models\Ticket;
use App\ModelStates\TicketState;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $ticket = Ticket::first();
    return view('welcome', compact('ticket'));
})->name('home');

Route::patch('/tickets/{id}/change-status', function ($id, Request $request) {
    $ticket = Ticket::find($id);
    $status = $request->get('status');

    $newStatus = TicketState::find($status, $ticket);
    $ticket->status->transitionTo($newStatus);

    return redirect()->route('home');
})->name('tickets.changeStatus');
