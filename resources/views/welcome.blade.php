Danh sách trạng thái có thể chuyển:
<ul>
    @foreach($ticket->getTransitionableStates() as $state)
        <li>
            <form id="{{ strtolower($state) }}" method="post" action="{{ route('tickets.changeStatus', $ticket) }}">
                @csrf
                @method('patch')
                <input name="status" value="{{ $state }}">
                <button>Chuyển trạng thái {{ $state }}</button>
            </form>
        </li>
    @endforeach
</ul>
