<?php

namespace App\Models;

use App\ModelStates\DevReviewTicketState;
use App\ModelStates\InProcessTicketState;
use App\ModelStates\NeedTestTicketState;
use App\ModelStates\OpenTicketState;
use App\ModelStates\ReOpenedTicketState;
use App\ModelStates\ResolvedTicketState;
use App\ModelStates\TestingTicketState;
use App\ModelStates\TicketState;
use Illuminate\Database\Eloquent\Model;
use Spatie\ModelStates\HasStates;

class Ticket extends Model
{
    use HasStates;

    protected $table = 'tickets';

    protected $fillable = [
        'title',
        'status',
    ];

    protected function registerStates(): void
    {
        $this->addState('status', TicketState::class)
            ->default(OpenTicketState::class)
            ->allowTransitions([
                [OpenTicketState::class, InProcessTicketState::class],
                [OpenTicketState::class, NeedTestTicketState::class],
            ])
            ->allowTransitions([
                [InProcessTicketState::class, DevReviewTicketState::class],
            ])
            ->allowTransitions([
                [DevReviewTicketState::class, NeedTestTicketState::class],
            ])
            ->allowTransitions([
                [NeedTestTicketState::class, TestingTicketState::class],
            ])
            ->allowTransitions([
                [TestingTicketState::class, ResolvedTicketState::class],
                [TestingTicketState::class, ReOpenedTicketState::class],
            ])
            ->allowTransitions([
                [ReOpenedTicketState::class, InProcessTicketState::class],
                [ReOpenedTicketState::class, NeedTestTicketState::class],
            ])
            ->allowTransitions([
                [ResolvedTicketState::class, ReOpenedTicketState::class],
            ]);
    }

    function getTransitionableStates()
    {
        return $this->status->transitionableStates();
    }
}
