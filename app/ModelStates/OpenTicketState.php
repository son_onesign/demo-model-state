<?php

namespace App\ModelStates;

class OpenTicketState extends TicketState
{
    public static $name = 'Open';
}