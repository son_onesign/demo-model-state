<?php

namespace App\ModelStates;

class ReOpenedTicketState extends TicketState
{
    public static $name = 'Re-Opened';
}