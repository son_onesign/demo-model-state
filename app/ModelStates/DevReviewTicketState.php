<?php

namespace App\ModelStates;

class DevReviewTicketState extends TicketState
{
    public static $name = 'Dev Review';
}