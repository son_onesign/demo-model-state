<?php

namespace App\ModelStates;

class ResolvedTicketState extends TicketState
{
    public static $name = 'Resolved';
}