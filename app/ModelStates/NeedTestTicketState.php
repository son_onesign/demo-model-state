<?php

namespace App\ModelStates;

class NeedTestTicketState extends TicketState
{
    public static $name = 'Need Test';
}