<?php

namespace App\ModelStates;

class InProcessTicketState extends TicketState
{
    public static $name = 'In Process';
}